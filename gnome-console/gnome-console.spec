%define glib2_version 2.52.0
%define gtk3_version 3.24.0
%define vte_version 0.67.90
%define desktop_file_utils_version 0.2.90
%define libhandy_version 1.5.90

Name:    console
Version: 42.beta
Release: 1%{?dist}
Summary: A simple user-friendly terminal emulator for the GNOME desktop.

License: GPLv3+
URL:     https://gitlab.gnome.org/GNOME/console
Source0: https://gitlab.gnome.org/GNOME/console/-/archive/%{version}/console-%{version}.tar.bz2



BuildRequires: pkgconfig(glib-2.0) >= %{glib2_version}
BuildRequires: pkgconfig(gsettings-desktop-schemas)
BuildRequires: pkgconfig(gtk+-3.0) >= %{gtk3_version}
BuildRequires: pkgconfig(libpcre2-8)
BuildRequires: pkgconfig(vte-2.91) >= %{vte_version}
BuildRequires: desktop-file-utils >= %{desktop_file_utils_version}
BuildRequires: gcc-c++
BuildRequires: gnome-shell
BuildRequires: meson
BuildRequires: sassc
BuildRequires: pkgconfig(libnautilus-extension)
BuildRequires: libhandy-devel%{?_isa} >= %{libhandy_version}
BuildRequires: libgtop2-devel

Requires: dbus
Requires: glib2%{?_isa} >= %{glib2_version}
Requires: gsettings-desktop-schemas
Requires: gtk3%{?_isa} >= %{gtk3_version}
Requires: vte291%{?_isa} >= %{vte_version}
Requires: libhandy%{?_isa} >= %{libhandy_version}

%description
A simple user-friendly terminal emulator for the GNOME desktop.

%global optflags %{optflags} -O3 -march=x86-64-v3 -mtune=generic

%prep
%setup


%build
%meson 

%meson_build

%install
%meson_install
%find_lang kgx --with-gnome


%check
desktop-file-validate %{buildroot}%{_datadir}/applications/org.gnome.Console.desktop
%meson_test

%files -f kgx.lang
%{_bindir}/kgx
%{_datadir}/metainfo/org.gnome.Console.metainfo.xml
%{_datadir}/applications/org.gnome.Console.desktop
%{_datadir}/dbus-1/services/org.gnome.Console.service
%{_datadir}/glib-2.0/schemas/org.gnome.Console.gschema.xml
%{_datadir}/icons/hicolor/scalable/apps/org.gnome.Console.svg
%{_datadir}/icons/hicolor/symbolic/apps/org.gnome.Console-symbolic.svg


%{_libdir}/nautilus/extensions-3.0/libkgx-nautilus.so





%changelog
* Sun Feb 20 2022 42
- 42 alpha

* Sat Feb 19 2022 initial spec file.
- No description
